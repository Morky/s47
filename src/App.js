// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// App Components
import AppNavBar from './components/AppNavBar';

// Page Components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';

// App Imports
import UserContext from './UserContext';

const App = ()=>{

	const [user, setUser] = useState(null);

	return (
		<UserContext.Provider value={{user, setUser}}>
			<BrowserRouter>
		  		<AppNavBar />
		  		<Routes>
		  			<Route path='/' element={<Home />} />
		    		<Route path='/courses' element={<Courses />} />
		    		<Route path='/register' element={<Register />} />
		    		<Route path='/login' element={<Login />} />
		    		<Route path='*' element={<NotFound />} />
		  		</Routes>
			</BrowserRouter>
    	</UserContext.Provider>
	);
}

export default App;